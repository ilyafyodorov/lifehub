requirejs.config({
    baseUrl: '/js',

    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap_toggle': ['jquery'],
        'threeControls': ['three'],
        'threeControls2': ['three'],        
        'stereoEffect': ['three'],
        'deviceOrientationControls': ['three']
    },

    paths: {
        jquery: 'bower/jquery/dist/jquery',
        bootstrap: 'bower/bootstrap/dist/js/bootstrap',
        bootstrap_toggle: 'bower/bootstrap-toggle/js/bootstrap-toggle',
        underscore: 'bower/underscore/underscore',
        backbone: 'bower/backbone/backbone',
        radio: 'bower/backbone.radio/build/backbone.radio',
        modelbinder: 'bower/backbone.modelBinder/Backbone.ModelBinder',
        three: 'bower/three.js/three',
        threeControls: 'libs/OrthographicTrackballControls',
        threeControls2: 'libs/TrackballControls',
        stereoEffect: 'libs/StereoEffect',
        deviceOrientationControls: 'libs/DeviceOrientationControls',
        hbs: 'bower/require-handlebars-plugin/hbs',
        text: 'bower/text/text',
        jstree: 'bower/jstree/dist/jstree'
    },

    deps: ['main']
});
