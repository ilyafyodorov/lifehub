define(function (require) {
    "use strict";

    var Backbone = require("backbone");

    var Router = Backbone.Router.extend({
        routes: {
            "*path": "browse"
        }
    });

    return Router;
});
