define( function (require) {
    'use strict';

    var Backbone = require('backbone');

    var resultsSelectionView = Backbone.View.extend({
        tagName: 'li',
        className: 'dropdown',

        template: require('hbs!./resultsSelectionView'),

        events: {
            'click li a': 'selectResult'
        },

        initialize: function () {
            this.listenTo(this.model, 'change:result', this.render);
        },

        render: function () {
            
            var resultList=[];
            var key;
            
            if(typeof(this.model.get('result')) !== 'undefined') {
                Object.keys(this.model.data.attributes).forEach(function(key) {
                    if ((key!='position')&&(key!='nodenum')&&(key!='index_quad')) {
                        resultList.push(key);
                    }
                });
                    
            }
            
            this.$el.html( this.template({
                selectedResult: this.model.get('result'),
                resultList: resultList.sort()
            }) );

            return this;
        },

        selectResult: function (e) {
            e.preventDefault();
            var $target = $(e.target);
            this.model.set('result', $target.attr('href'));
        }

    });

    return resultsSelectionView;
});
