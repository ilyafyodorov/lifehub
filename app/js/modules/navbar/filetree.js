//this module defines the Model for Filetree
define( function (require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require('radio').channel("app");
    var fileTree = Backbone.Model.extend({

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        initialize: function () {
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        //load file at particular path
        load: function (path) {
            if (path) {

                //the following lines delete /MODEL/ from the path, it will be replaced by /data/ in the viewport model loader
                path = path.split("/");
                path.splice(0,1);
                path = path.join("/");

                radio.trigger('select:model', path);
            }
        }

    });

    return fileTree;

});
