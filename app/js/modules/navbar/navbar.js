// this module defines Navbar - a View that shows the file tree inside the Browse modal
// is called by main.js which creates a Navbar on initialization
// Corresponding Model is in filetree.js
define(function (require) {
    "use strict"

    var Backbone = require("backbone");
    var radio = require('radio').channel("app");

    var counter = 0;

    // key requirement - js Tree library which creates file tree from user data
    require("jstree");

    var fileTree  = require('./filetree');

    var Navbar = Backbone.View.extend({

        events: {
            // definitions of actions in case of jstree manipulation (selection of folders and files)
            'select_node.jstree': 'manipulate'
        },

        initialize: function () {

            //get the url of data root (defined in main.js)
            this.url = radio.request("data:root");

            this.$el.jstree({
                'core' : {
                    //definition of jstree contents via "callback function" method - see jstree documentation
                    'data' : this.loadNode.bind(this),
                    'themes': { responsive: true, dots: false}
                },

                // plugin usage: "types" to be able to define different types of data in the tree (file/folder etc.)
                plugins : [ "types" ],

                types : {
                    //2 types defined: 'dir' - for directories, 'file' - for files
                    'dir' : {
                        "icon" : "glyphicon glyphicon-th-large"
                    },
                    'file' : {
                        "icon" : "glyphicon glyphicon-file"
                    }
                }

            });

        },

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        // jstree callback function, sends XHR and parses data from server
        loadNode: function (obj, cb) {

            var xhr = new XMLHttpRequest();

            if(obj.id === "#") {
                xhr.open('GET', this.url, true);
            } else {
                //get full path to selected node in the tree
                var objPath = this.$el.jstree(true).get_path(obj,"/");
                xhr.open('GET', this.url+'/'+objPath, true);
            }

            var jsonData=[];

            xhr.onload = function(event) {
                //parse result
                var jsonInput = JSON.parse(xhr.responseText);

                //initialize tree with response from server
                for (var k in jsonInput.items) {
                    var parseObj=jsonInput.items[k];
                    if (typeof(parseObj)=='object') {

                        var haveChildren = false;
                        if (parseObj.cls=='dir') {
                            haveChildren=true;
                        }

                        jsonData.push({
                            "text": parseObj.val,
                            "id": counter+1,
                            "children": haveChildren,
                            "type": parseObj.cls,
                            "state": {selected: false}
                        });

                        counter = counter+1;
                    }
                }

                cb(jsonData);
            }

            xhr.send();

        },

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        //this function handles jstree nodes selection
        manipulate: function (e, data) {
            var objNode = data.node;

            //if a directory is clicked, show/hide its contents
            if (objNode.type=='dir') {
                this.$el.jstree().toggle_node(objNode);
            }

            //if a file is clicked, it should be opened in the viewport
            if (objNode.type=='file') {
                var filePath = data.instance.get_path(objNode,'/');
                //update the address line in browser so that a link can be shared
                radio.trigger('navigate', 'MODEL/'+filePath);
                //send model url link to viewport script (viewport/model.js)
                radio.trigger('select:model', filePath);
                //hide the modal
                $('#browseModal').modal('hide');
            }

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

    });

    return Navbar;
});
