define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        tagname: "div",
        className: "query-window",

        template: require("hbs!./query"),

        initialize: function() {
            radio.on("node:selected", this.render, this);
        },

        render: function (vIndex) {
            if (vIndex) {

                var queryData = this.model.queryNode(vIndex);
                this.$el.html(this.template({
                    nodenum: queryData.nodenum,
                    resultList: queryData.resultList
                }));
                this.$el.show();
            } else {
                this.$el.hide();
            };
            return this;
        },

        remove: function () {
            radio.off("node:selected", this.render);
            Backbone.View.prototype.remove.apply(this, arguments);
            return this;
        }
    });

    return View;
});
