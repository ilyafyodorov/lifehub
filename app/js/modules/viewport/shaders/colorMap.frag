uniform vec3 diffuseColor;
uniform float EDGES_MODE;

uniform int RESULT;
uniform int NODES;
uniform int TRANSPART;
uniform int LIGHTS;

uniform sampler2D colorMap;
uniform float delta;
uniform vec3 minColor;
uniform vec3 maxColor;

uniform float viewcutX;
uniform float viewcutY;
uniform float viewcutZ;

uniform float Xrev;
uniform float Yrev;
uniform float Zrev;

varying float vValue;
varying vec3 vPosition;
varying vec3 totalLight;

#ifdef BARYCENTRIC
  varying vec3 vBC;
#endif

void main(void) {

  vec4 color = vec4(diffuseColor, 0.5);

  if (RESULT == 1) {
    color = texture2D(colorMap, vec2(vValue, 0.5));

    float eps = min( 0.1, fwidth(vValue) );

    #ifdef USE_MIN_COLOR
      if (vValue < 0. - eps) color = vec4(minColor, 1);
    #endif
    #ifdef USE_MAX_COLOR
      if (vValue > 1. + eps) color = vec4(maxColor, 1);
    #endif

  };

  if (Xrev==0.) {
      if (vPosition.x > viewcutX ) discard;
  };

  if (Xrev==1.) {
      if (vPosition.x < viewcutX ) discard;
  };

  if (Yrev==0.) {
      if (vPosition.y > viewcutY ) discard;
  };

  if (Yrev==1.) {
      if (vPosition.y < viewcutY ) discard;
  };

  if (Zrev==0.) {
      if (vPosition.z > viewcutZ) discard;
  };

  if (Zrev==1.) {
      if (vPosition.z < viewcutZ) discard;
  };

  #ifdef BARYCENTRIC
    vec3 dBC = fwidth(vBC);
  #endif

  if (EDGES_MODE == 2.) {
    if (any(lessThan(vBC, dBC/2.))) color.rgb = vec3(0.);
  };

  if (NODES == 1) {
    if (any(greaterThan(vBC, vec3(1) - dBC))) color.rgb = vec3(1.) - color.rgb;
  };

  if (LIGHTS == 1) {
      color.rgb *= totalLight;
  };
  
  if (TRANSPART == 1) {
      if ( gl_FrontFacing ) {
          gl_FragColor = color;
      } else {
          discard;
      };
  } else {
      gl_FragColor = ( gl_FrontFacing ) ? vec4(0.6, 0.6, 0.6, 0.0) : color;
  };
  
}