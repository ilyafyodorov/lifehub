
  attribute float value;

  uniform float minContourValue;
  uniform float maxContourValue;
  uniform int RESULT;

  uniform int LIGHTS;

  varying float vValue;
  varying vec3 vPosition;

  uniform vec3 ambientLightColor;

  #if NUM_DIR_LIGHTS > 0

    struct DirectionalLight {
      vec3 direction;
      vec3 color;

      int shadow;
      float shadowBias;
      float shadowRadius;
      vec2 shadowMapSize;
    };

    uniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];
  #endif

  varying vec3 totalLight;

#ifdef BARYCENTRIC
  attribute vec3 barycentric;
  varying vec3 vBC;
#endif

void main() {
  if(RESULT == 1) {
    vValue = (value - minContourValue)/(maxContourValue - minContourValue);
  }

  vPosition = position;

  #ifdef BARYCENTRIC
    vBC = barycentric;
  #endif

  if (LIGHTS == 1) {
    totalLight = vec3(0.0);

    totalLight.rgb += ambientLightColor;

    #if NUM_DIR_LIGHTS > 0
      for ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {
        DirectionalLight directLight = directionalLights[i];
        totalLight += max(dot(normalMatrix*normal, directLight.direction), 0.)*directLight.color;
      }
    #endif

    totalLight = clamp(totalLight, 0., 1.);
  };

  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
