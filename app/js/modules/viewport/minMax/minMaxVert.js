define( function (require) {
    "use strict";

    var Backbone = require("backbone");
    var THREE = require('three');
    
    var DPR = window.devicePixelRatio;

    var View = Backbone.View.extend({

        className: "minMaxVert",

        vertexNum: null,

        name: '',
        
        initialize: function() {
          this.listenTo(this.model, "change:result", this.render);
        },        

        render: function () {
            if ( this.model.get('result') ) {
                this.prepareMax().$el.show();
            } else {
                this.$el.hide();
            };

            return this;
        },

        prepareMax: function () {

            var attrName = this.model.get("result");

            var maxValue = this.model.data.getAttribute(attrName).array[ this.vertexNum ];
            var title = this.name + maxValue.toExponential(3);

            this.$el.text(title);

            return this;
        },

        /**
         * Move label according to given arrow, such that arrow goes
         * through label center and starts at label boundary
         * @param {THREE.Vector3} origin - arrow origin
         * @param {THREE.Vector3} direction - arrow direction
         */
        translate: function(origin, direction) {

            var w = this.$el.width() / 2;
            var h = this.$el.height() / 2;

            var center = new THREE.Vector3(w, h, 0);
            var offset = new THREE.Vector3();
            offset.x = THREE.Math.clamp(h * direction.x / Math.abs(direction.y), -w, w);
            offset.y = THREE.Math.clamp(w * direction.y / Math.abs(direction.x), -h, h);

            origin.sub(center).sub(offset);

            this.el.style.left = Math.floor(origin.x/DPR) + 'px';
            this.el.style.top = Math.floor(origin.y/DPR) + 'px';
        }

    });

    return View;
} );
