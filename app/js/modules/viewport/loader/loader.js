define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        tagname: "div",
        className: "progress",

        template: require("hbs!./loader"),

        initialize: function () {
            radio.on("model:progress", this.showProgress, this);
            radio.on("model:loaded", this.success, this);
        },

        showProgress: function (progress) {
            this.$(".progress-bar").css("transition", 'none');
            this.$(".progress-bar").css('width', progress * 100 +'%');
        },

        success: function () {
            this.$(".progress-bar").css("width", "100%");
            this.$(".progress-bar").addClass("progress-bar-success");
            this.$(".progress-bar").html("Please wait...");
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        remove: function () {
            radio.off("model:progress", this.showProgress);
            radio.off("model:loaded", this.success);
            Backbone.View.prototype.remove.apply(this, arguments);
            return this;
        }
    });

    return View;
});
