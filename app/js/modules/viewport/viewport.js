define(function(require) {
    "use strict";

    var $ = require("jquery");
    var _ = require("underscore");
    var Backbone = require("backbone");
    var radio = require("radio").channel("app");
    var THREE = require("three");
    require('threeControls');
    require('threeControls2');    
    require('stereoEffect');

    var Loader = require("./loader/loader");
    var Options = require("./options/options");
    var Query = require("./query/query");
    var Legend = require("./legend/legend");
    var minMaxVert = require("./minMax/minMaxVert");

    var INTERSECTED = null;
    var HIGHLIGHT_SIZE = 1;
    var HIGHLIGHT_THRESHOLD = 1;
    var mouse = new THREE.Vector2();

    var sceneSize;
    var modelCenter;
    
    var time = 0;


    var Viewport = Backbone.View.extend({

        events: {
            "mousemove canvas": "updateMouse",
            "click canvas": "selectNode",
            "contextmenu canvas": "selectRotationCenter"
        },
        
        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
            this.loader = new Loader();
            this.$el.append(this.loader.render().$el);
            this.onResize = this.onResize.bind(this);
            $(window).on('resize', this.onResize);
        },

        remove: function() {
            cancelAnimationFrame(this.animationFrame);

            $(window).off("resize", this.onResize);
            this.stopListening();
            this.undelegateEvents();

            this.options.remove();
            this.query.remove();
            this.legend.remove();
            this.minVert.remove();
            this.maxVert.remove();
            
            for (var i = this.scene.children.length - 1; i >= 0; --i){
                var child = this.scene.children[i];
                if (child.parent) child.parent.remove(child);
                if (child.geometry) child.geometry.dispose();
                if (child.material) child.material.dispose();
            };

            this.$(this.renderer.domElement).remove();
            this.renderer.dispose();
            this.controls.dispose();
            
            return this;
        },

        speedSetVR: function (x) {
            if((x<-180)||(x>180)) return 0;
            if((x>=-180)&&(x<-120)) return (x+180)/(-120+180);
            if((x>=-120)&&(x<-60)) return 1;
            if((x>=-60)&&(x<0)) return 1-(x+60)/(60);
            if((x>=0)&&(x<60)) return -(x)/(60);
            if((x>=60)&&(x<120)) return -1;
            if((x>=120)&&(x<180)) return -(1-(x-120)/(60));
        },
        
        changeViewVR: function() {
            
            if(this.model.attributes.VRCheck == 1) {



                $('#topnav').addClass("removed");
                
                
                this.scene.remove(this.markerMax);
                this.scene.remove(this.markerMin);
                this.scene.remove(this.markerSelect);
            
                this.query.remove();
                this.minVert.remove();
                this.maxVert.remove();
            
                this.camera = new THREE.PerspectiveCamera( 50, this.$el.width() / this.$el.height(), 1, sceneSize*200.0 );
                this.camera.position.set(modelCenter.x,modelCenter.y,modelCenter.z);
                this.camera.position.z += sceneSize*2;
                
                this.effect = new THREE.StereoEffect(this.renderer);
                this.effect.setSize( this.$el.width(), this.$el.height() );                

                this.controls = new THREE.TrackballControls( this.camera, this.renderer.domElement);
                this.controls.target.copy(modelCenter);
                this.controls.staticMoving = true;

                //var lockFunction =  window.screen.orientation.lock;
                //if (lockFunction.call(window.screen.orientation, 'landscape')) {
                //           console.log('Orientation locked');
                //        } else {
                //            console.error('There was a problem in locking the orientation');
                //        }                
                if (document.getElementById("js-viewport").webkitRequestFullscreen) {
                    document.getElementById("js-viewport").webkitRequestFullscreen();
                }
                //$('body').webkitRequestFullscreen();
                //window.screen.orientation.lock("landscape-primary");


            } else {
                
                $('#topnav').removeClass("removed");
                
                this.model.set("result", '');
                
                cancelAnimationFrame(this.animationFrame);

                //this.options.remove();
                this.query.remove();
                this.legend.remove();
                this.minVert.remove();
                this.maxVert.remove();
                
                for (var i = this.scene.children.length - 1; i >= 0; --i){
                    var child = this.scene.children[i];
                    if (child.parent) child.parent.remove(child);
                    if (child.geometry) child.geometry.dispose();
                    if (child.material) child.material.dispose();
                };

                
                this.renderer.dispose();
                this.render();
                
            }


            
        },
        
        // Our preferred controls via DeviceOrientation
        setOrientationControls: function (e) {
              if (!e.alpha) {
                return;
              }
              
              this.model.attributes.alpha = e.alpha;
              this.model.attributes.beta = e.beta;
              this.model.attributes.gamma = e.gamma;
              
              
              //$('#trackAngle').empty();
              //$('#trackAngle').append(this.model.attributes.alpha.toString()+' '+this.model.attributes.beta.toString()+' '+this.model.attributes.gamma.toString()); 
            
        },
        
        
        render: function () {

            sceneSize = this.model.mesh.geometry.boundingSphere.radius;
            HIGHLIGHT_SIZE = sceneSize/100.0;
            HIGHLIGHT_THRESHOLD = sceneSize/200.0;
            modelCenter = this.model.mesh.geometry.boundingSphere.center;

            // SCENE
            this.scene = new THREE.Scene();

            // GEOMETRY
            this.scene.add(this.model.mesh);

            // CAMERA
            
            
            //Orthographic                       
            this.camera = new THREE.OrthographicCamera(
                -sceneSize * this.$el.width() / this.$el.height(),
                sceneSize * this.$el.width() / this.$el.height(),
                sceneSize,
                -sceneSize,
                -10*sceneSize,
                10*sceneSize
            );
            this.camera.zoom = 1;
            this.camera.position.set(sceneSize,sceneSize,sceneSize).add(modelCenter);
            this.camera.lookAt(modelCenter);
            this.camera.updateProjectionMatrix();
            this.scene.add(this.camera);
             
            // RENDERER
            
            this.renderer = new THREE.WebGLRenderer({ antialias: true });
            this.renderer.setSize( this.$el.width(), this.$el.height() );
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.setClearColor(0xffffff);
            this.$el.append(this.renderer.domElement);            
            

            // CONTROLS
            this.controls = new THREE.OrthographicTrackballControls( this.camera, this.renderer.domElement);
            this.controls.target.copy(modelCenter);
            this.controls.staticMoving = true;
            
            // VR: add device orientation control
            window.addEventListener('deviceorientation', this.setOrientationControls.bind(this), true);            

            // LIGHT
            this.light = new THREE.AmbientLight( 0x909090 ); // soft white light
            this.scene.add(this.light);

            this.directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
            this.directionalLight.position.set( 1, 1, 1 ).normalize();
            this.scene.add( this.directionalLight );

            this.directionalLight2 = new THREE.DirectionalLight( 0xffffff, 0.5 );
            this.directionalLight2.position.set( -1, -1, -1 ).normalize();
            this.scene.add( this.directionalLight2 );

            // RAYCASTER
            this.raycaster = new THREE.Raycaster();
            this.raycaster.params.Points.threshold = HIGHLIGHT_THRESHOLD;

            // Highlight marker
            this.markerHighlight = new THREE.Mesh(
                new THREE.SphereGeometry( HIGHLIGHT_SIZE, 32, 32 ),
                new THREE.MeshBasicMaterial( {color: 0xffaa00, depthTest: false} )
            );
            this.markerHighlight.visible = false;
            this.scene.add(this.markerHighlight);

            // Selection marker
            this.markerSelect = new THREE.Mesh(
                new THREE.SphereGeometry( HIGHLIGHT_SIZE, 32, 32 ),
                new THREE.MeshBasicMaterial( {color: 0xff0000, depthTest: false} )
            );
            this.markerSelect.visible = false;
            this.scene.add(this.markerSelect);

            // Rotation center marker
            this.rotationCenter = new THREE.AxisHelper( HIGHLIGHT_SIZE*5 );
            this.rotationCenter.position.copy(modelCenter);
            this.rotationCenter.material.depthTest = false;
            this.scene.add(this.rotationCenter);

            //Max value location markers

            this.markerMax = new THREE.ArrowHelper(
                new THREE.Vector3( 1, 0, 0),
                new THREE.Vector3( 0, 0, 0 ),
                HIGHLIGHT_SIZE*30,
                0x111111
            );
            this.markerMax.line.material.depthTest = false;
            this.markerMax.visible = false;
            this.scene.add(this.markerMax);

            this.markerMin = new THREE.ArrowHelper(
                new THREE.Vector3( 1, 0, 0),
                new THREE.Vector3( 0, 0, 0 ),
                HIGHLIGHT_SIZE*30,
                0x111111
            );
            this.markerMin.line.material.depthTest = false;
            this.markerMin.visible = false;
            this.scene.add(this.markerMin);

            this.listenTo(this.model, "change:result", this.minMaxUpdate);

            // Add widgets
            if (typeof(this.options) == 'undefined') {
                this.options = new Options({model: this.model});
                this.$el.append(this.options.render().$el);
                this.listenTo(this.options, 'minMaxNeedsUpdate', this.minMaxUpdate);
            }

            this.query = new Query({model: this.model});
            this.$el.append(this.query.render().$el);
            
            //this.$el.append('<div class="angle-window" id="trackAngle"></div>');           

            this.legend = new Legend({model: this.model});
            this.$el.append( this.legend.$el );
            this.legend.render();

            this.minVert = new minMaxVert({model: this.model});
            this.minVert.name = 'Min: ';
            this.$el.append( this.minVert.render().$el );

            this.maxVert = new minMaxVert({model: this.model});
            this.maxVert.name = 'Max: ';
            this.$el.append( this.maxVert.render().$el );

            this.loader.remove();
            
            this.listenTo(this.model, "change:VRCheck", this.changeViewVR);                        
            
            this.animate();

        },
        
        // triangle wave function
        triangle: function(t,a) {
            
            return 1/a*(t-a*Math.floor(t/a+1/2))*Math.pow(-1,Math.floor(t/a+1/2));
            
        },

        animate: function () {
            
            if(this.model.attributes.VRCheck == 1) {
                
                
                this.animationFrame = requestAnimationFrame( this.animate.bind(this) );
                this.controls.update();

                var scale = 1/this.camera.zoom;
                
                //rotate camera is corresponding checkbox is selected
                if(this.model.attributes.animateCheck==1) {
                    var d = new Date();
                    var timeNow = d.getTime()/1000;
                    var x = this.camera.position.x - modelCenter.x,
                        y = this.camera.position.y - modelCenter.y,
                        z = this.camera.position.z - modelCenter.z;
                    var speed;
                    if(this.model.attributes.VRCheck == 1) {
                        speed = 0.01;
                        if (this.model.attributes.beta !== -1000) {
                            speed *= this.speedSetVR(this.model.attributes.beta);
                        }
                    } else {
                        speed = 0.01;
                    }
                    this.camera.position.x = modelCenter.x + x * Math.cos(speed) + z * Math.sin(speed);
                    this.camera.position.z = modelCenter.z + z * Math.cos(speed) - x * Math.sin(speed);
                    this.camera.lookAt(modelCenter);
                }
                
                

                this.model.mesh.material.needsUpdate = true;

                this.updateArrows(scale);

                //this.renderer.render(this.scene, this.camera);
                this.effect.render(this.scene, this.camera);
            
            } else {

                this.animationFrame = requestAnimationFrame( this.animate.bind(this) );
                this.controls.update();
                this.checkHighlight();

                var scale = 1/this.camera.zoom;
                
                //rotate camera is corresponding checkbox is selected
                if(this.model.attributes.animateCheck==1) {
                    var d = new Date();
                    var timeNow = d.getTime()/1000;
                    var x = this.camera.position.x - modelCenter.x,
                        y = this.camera.position.y - modelCenter.y,
                        z = this.camera.position.z - modelCenter.z;
                    var speed = 0.01;
                    this.camera.position.x = modelCenter.x + x * Math.cos(speed) + z * Math.sin(speed);
                    this.camera.position.z = modelCenter.z + z * Math.cos(speed) - x * Math.sin(speed);
                    this.camera.lookAt(modelCenter);
                }

                // calculate highlight threshold based on camera zoom and assign it to raycaster
                HIGHLIGHT_THRESHOLD = this.model.mesh.geometry.boundingSphere.radius * (0.00333+(scale-1)/(0.02-1)*(0.000333-0.00333));
                this.raycaster.params.Points.threshold = HIGHLIGHT_THRESHOLD;
                this.markerSelect.scale.set(scale, scale, scale);
                this.markerHighlight.scale.set(scale, scale, scale);

                this.model.mesh.material.needsUpdate = true;

                this.updateArrows(scale);

                this.renderer.render(this.scene, this.camera);
            
            }
        },

        updateMouse: function (event) {
            mouse.x = ( event.offsetX / this.$el.width() ) * 2 - 1;
            mouse.y = - ( event.offsetY / this.$el.height() ) * 2 + 1;
        },

        /**
         * Check if point is visible under current viewcuts
         * @param {THREE.Vector3} point - point to check for visibility
         */
        _isVisible: function (point) {

            var show = true;

            if (this.model.attributes.Xrev==false) {
                if (point.x > this.model.attributes.viewcutX ) show = false;
            };

            if (this.model.attributes.Xrev==true) {
                if (point.x < this.model.attributes.viewcutX ) show = false;
            };

            if (this.model.attributes.Yrev==false) {
                if (point.y > this.model.attributes.viewcutY ) show = false;
            };

            if (this.model.attributes.Yrev==true) {
                if (point.y < this.model.attributes.viewcutY ) show = false;
            };

            if (this.model.attributes.Zrev==false) {
                if (point.z > this.model.attributes.viewcutZ) show = false;
            };

            if (this.model.attributes.Zrev==true) {
                if (point.z < this.model.attributes.viewcutZ) show = false;
            };

            return show;
        },

        checkHighlight: function () {
            this.raycaster.setFromCamera(mouse, this.camera);
            var intersectedNodes = this.raycaster.intersectObject(this.model.nodes);
            var intersectedElements = this.raycaster.intersectObject(this.model.mesh);

            intersectedNodes = _.filter(intersectedNodes,
                                        function (val) {
                                            return this._isVisible(val.point);
                                        }.bind(this));
            intersectedElements = _.filter(intersectedElements,
                                           function (val) {
                                               return this._isVisible(val.point);
                                           }.bind(this));

            if (intersectedElements.length && intersectedNodes.length) {
                if ( Math.abs(intersectedElements[0].distance - intersectedNodes[0].distance) < HIGHLIGHT_THRESHOLD ) {
                    var pointIndex = intersectedNodes[0].index;
                    var positionArray = intersectedNodes[0].object.geometry.attributes.position.array;
                    var point = new THREE.Vector3().fromArray(positionArray, 3*pointIndex);

                    this.markerHighlight.position.copy(point);
                    this.markerHighlight.visible = true;
                    INTERSECTED = pointIndex;
                    return;
                }
            }

            this.markerHighlight.visible = false;
            INTERSECTED = null;
        },

        selectNode: function () {
            if (INTERSECTED === null) {
                this.markerSelect.visible = false;
                radio.trigger("node:selected");
                this.model.set("selectedNode",undefined);
            } else {
                radio.trigger("node:selected", INTERSECTED);
                this.markerSelect.position.copy(this.markerHighlight.position);
                this.markerSelect.visible = true;
                this.model.set("selectedNode",INTERSECTED);
            };
        },

        selectRotationCenter: function (event) {
            if (!event.ctrlKey) return;

            if (INTERSECTED === null) {
                this.rotationCenter.position.copy(this.model.mesh.geometry.boundingSphere.center);
                this.camera.position.add(this.rotationCenter.position).sub(this.controls.target);
                this.controls.target.copy(this.rotationCenter.position);
            } else {
                this.rotationCenter.position.copy(this.markerHighlight.position);
                this.camera.position.add(this.rotationCenter.position).sub(this.controls.target);
                this.controls.target.copy(this.rotationCenter.position);
            };
        },

        onResize: function () {
            requestAnimationFrame( this.handleResize.bind(this) );
        },

        handleResize: function () {
            var sceneSize = this.model.mesh.geometry.boundingSphere.radius;
            if(this.model.attributes.VRCheck == 1) {
                
                this.effect.setSize( this.$el.width(), this.$el.height() );
                
            } else {
                
                this.renderer.setSize( this.$el.width(), this.$el.height() );
                this.camera.left = -sceneSize * this.$el.width() / this.$el.height();
                this.camera.right = sceneSize * this.$el.width() / this.$el.height();
                this.camera.updateProjectionMatrix();
                this.controls.handleResize();               
                
            }
                
                
            this.legend.render();
        },

        /**
         * Function to find min and max highlighting locations
         * Triggered by Options view
         */
        minMaxUpdate: function() {

            var attrName = this.model.get("result");
            if (!attrName) {
                this.markerMax.visible = false;
                this.markerMin.visible = false;
                return;
            }
            var array = this.model.data.getAttribute(attrName).array;
            var nodeCoor = this.model.nodes.geometry.attributes.position.array;
            var vertexMin;
            var vertexMax;

            //find Frustum - visible part of viewport based on camera data
            this.camera.updateMatrix();
            this.camera.updateMatrixWorld();
            var frustum = new THREE.Frustum();
            frustum.setFromMatrix( new THREE.Matrix4().multiplyMatrices(this.camera.projectionMatrix, this.camera.matrixWorldInverse ) );

            var atest;
            var inView = 0;  //counter of nodes in view

            for ( var i = 0; i < array.length; i++ )
            {
                //check if a node is within the viewport
                atest = new THREE.Vector3( nodeCoor[i*3], nodeCoor[i*3+1], nodeCoor[i*3+2]);

                if (frustum.containsPoint(atest)) {

                    //check if a node is visible according to viewcuts
                    if ( this._isVisible(atest) ) {

                        //TODO: check if a node is not occluded by mesh (raycaster is too slow)

                        if(inView == 0) {
                            vertexMin = i;
                            vertexMax = i;
                        };

                        inView = inView + 1;

                        if (array[i] <= array[vertexMin]) {
                            vertexMin = i;
                        };

                        if (array[i] >= array[vertexMax]) {
                            vertexMax = i;
                        };

                    };

                };

            };

            //if at least 3 nodes are visible
            if(inView>2) {

                //vertices for current display min/max (not global min/max)
                this.maxVert.vertexNum = vertexMax;
                this.minVert.vertexNum = vertexMin;
                this.maxVert.render();
                this.minVert.render();

                this.markerMax.visible = true;
                this.markerMin.visible = true;
            };

        },

        /**
         * Function to update min and max highlighting locations (arrows, labels)
         */

        updateArrows: function(scale) {

            if(this.markerMax.visible && this.markerMin.visible) {

                var vertexMin = this.minVert.vertexNum;
                var vertexMax = this.maxVert.vertexNum;
                var nodeCoor = this.model.nodes.geometry.attributes.position.array;

                var arrLength = HIGHLIGHT_SIZE*30*scale;

                // calculate max arrow origin and direction
                var targetMax = new THREE.Vector3().fromArray(nodeCoor, 3*vertexMax);
                var dirMax = this.rotationCenter.position.clone().sub(targetMax);

                if (dirMax.length() < HIGHLIGHT_SIZE) { // too close to rotation center
                    dirMax = this.model.mesh.geometry.boundingSphere.center.clone().sub(targetMax);
                }

                dirMax.normalize();
                var origMax = targetMax.clone().addScaledVector(dirMax, -arrLength);

                // calculate min arrow origin and direction
                var targetMin = new THREE.Vector3().fromArray(nodeCoor, 3*vertexMin);
                var dirMin = this.rotationCenter.position.clone().sub(targetMin);

                if (dirMin.length() < HIGHLIGHT_SIZE) { // too close to rotation center
                    dirMin = this.model.mesh.geometry.boundingSphere.center.clone().sub(targetMin);
                }

                dirMin.normalize();
                var origMin = targetMin.clone().addScaledVector(dirMin, -arrLength);

                // set arrows origins and directions
                this.markerMax.position.copy(origMax);
                this.markerMax.setDirection(dirMax);
                this.markerMax.scale.set(scale, scale, scale);

                this.markerMin.position.copy(origMin);
                this.markerMin.setDirection(dirMin);
                this.markerMin.scale.set(scale, scale, scale);

                // transformation matrix from WebGL clipspace to DOM element space
                var widthHalf = 0.5*this.renderer.context.canvas.width;
                var heightHalf = 0.5*this.renderer.context.canvas.height;

                var clipspaceToScreen = new THREE.Matrix4().set(
                    widthHalf, 0, 0, widthHalf + 15,
                    0, -heightHalf, 0, heightHalf,
                    0, 0, 1, 0,
                    0, 0, 0, 1
                );

                // move max label
                origMax.project(this.camera).applyMatrix4(clipspaceToScreen);
                targetMax.project(this.camera).applyMatrix4(clipspaceToScreen).sub(origMax);

                this.maxVert.translate(origMax, targetMax);

                // move min label
                origMin.project(this.camera).applyMatrix4(clipspaceToScreen);
                targetMin.project(this.camera).applyMatrix4(clipspaceToScreen).sub(origMin);

                this.minVert.translate(origMin, targetMin);

            };
        }

    });

    return Viewport;
});
