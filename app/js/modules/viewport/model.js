define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require('radio').channel("app");
    var THREE = require("three");
    var msgpack = require("/js/libs/msgpack.js");

    var EDGES_MODES = {
        "no edges": 0,
        "hard edges": 1,
        "mesh edges": 2
    };
    var DEFAULT_COLOR_MAP = [
        new THREE.Color(0.164, 0.043, 0.850),
        new THREE.Color(0.150, 0.306, 1.000),
        new THREE.Color(0.250, 0.630, 1.000),
        new THREE.Color(0.450, 0.853, 1.000),
        new THREE.Color(0.670, 0.973, 1.000),
        new THREE.Color(0.880, 1.000, 1.000),
        new THREE.Color(1.000, 1.000, 0.750),
        new THREE.Color(1.000, 0.880, 0.600),
        new THREE.Color(1.000, 0.679, 0.450),
        new THREE.Color(0.970, 0.430, 0.370),
        new THREE.Color(0.850, 0.150, 0.196),
        new THREE.Color(0.650, 0.000, 0.130)
    ];

    var Model = Backbone.Model.extend({

        _defaults: { // Backbone's defaults won't trigger the events, done manually during setup
            "showPart": true,
            "edges": "no edges",
            "nodes": false,
            "transparentPart": false,
            
            "animateCheck": 0,
            "VRCheck": 0,

            "result": '',
            "minResultVertex": null,
            "maxResultVertex": null,
            
            "selectedNode": undefined,

            "colorMap": DEFAULT_COLOR_MAP,
            "minColor": new THREE.Color(0.2, 0.2, 0.2),
            "maxColor": new THREE.Color(0.8, 0.8, 0.8),
            "minContourValue": '',
            "maxContourValue": '',

            "lights": true,

            "viewcutX": null,
            "viewcutY": null,
            "viewcutZ": null,
            "maxX" : null,
            "maxY" : null,
            "maxZ" : null,
            "minX" : null,
            "minY" : null,
            "minZ" : null,
            "Xrev"  : null,
            "Yrev"  : null,
            "Zrev"  : null,

            "rangeX": 100,
            "rangeY": 100,
            "rangeZ": 100,
            "cboxX" : false,
            "cboxY" : false,
            "cboxZ" : false,
            
            "alpha" :-1000,
            "beta" : -1000,
            "gamma" :-1000,
            
            "VRCheck" : 0

        },

        initialize: function() {
            this.urlRoot = radio.request("data:root");
            this.fetch();

            this.on({
                "change:showPart": function () {
                    this.mesh.visible = this.get("showPart");
                },
                "change:edges": function (model, value) {
                    this.showEdges(value);
                },
                "change:nodes": function (model, value) {
                    this.showNodes(value);
                },
                "change:transparentPart": function (model, value) {
                    this.showTransparentPart(value);
                },

                "change:result": function (model, value) {
                    this.setResult(value);
                },
                "change:minResultVertex": function () {
                    var attrName = this.get("result");
                    if (!attrName) return;
                    var minVal = this.data.getAttribute(attrName).array[ this.get("minResultVertex") ];
                    this.set("minContourValue",minVal);
                },
                "change:maxResultVertex": function () {
                    var attrName = this.get("result");
                    if (!attrName) return;
                    var maxVal = this.data.getAttribute(attrName).array[ this.get("maxResultVertex") ];
                    this.set("maxContourValue",maxVal);
                },

                "change:colorMap": function (model, value) {
                    this.setColorMap(value);
                },
                "change:minColor": function (model, value) {
                    this.setMinColor(value);
                },
                "change:maxColor": function (model, value) {
                    this.setMaxColor(value);
                },
                "change:minContourValue": function (model, value) {
                    this.setMinContourValue(value);
                },
                "change:maxContourValue": function (model, value) {
                    this.setMaxContourValue(value);
                },

                "change:lights": function (model, value) {
                    this.showLights(value);
                },
                "change:rangeX change:rangeY change:rangeZ change:cboxX change:cboxY change:cboxZ": function (model, value) {

                    var Xperc = model.attributes.rangeX;
                    var Yperc = model.attributes.rangeY;
                    var Zperc = model.attributes.rangeZ;

                    var Xrev = model.attributes.cboxX;
                    var Yrev = model.attributes.cboxY;
                    var Zrev = model.attributes.cboxZ;

                    model.attributes.viewcutX =  model.attributes.minX+Xperc/100.0*(model.attributes.maxX-model.attributes.minX);
                    model.attributes.viewcutY =  model.attributes.minY+Yperc/100.0*(model.attributes.maxY-model.attributes.minY);
                    model.attributes.viewcutZ =  model.attributes.minZ+Zperc/100.0*(model.attributes.maxZ-model.attributes.minZ);

                    model.attributes.Xrev = Xrev;
                    model.attributes.Yrev = Yrev;
                    model.attributes.Zrev = Zrev;

                    model.setViewcuts(model.attributes.viewcutX,model.attributes.viewcutY,model.attributes.viewcutZ);
                    model.setViewcutRevs(model.attributes.Xrev,model.attributes.Yrev,model.attributes.Zrev);
                }
            }, this);
        },

        sync: function(method, model, options) {

            var xhr = new XMLHttpRequest();
            xhr.open('GET', model.url(), true);
            xhr.responseType = 'arraybuffer';

            xhr.onload = function(event) {
                radio.trigger("model:loaded");
                setTimeout(function () {
                    var decoded = msgpack.decode(xhr.response);
                    var loader = new THREE.BufferGeometryLoader();
                    model.setup( loader.parse( decoded ), decoded.model_name, decoded.comment, decoded.material_data );
                    options.success();
                }, 40);
            };

            xhr.onprogress = function(event) {
                radio.trigger("model:progress", event.loaded/event.total);
            };

            xhr.send();
        },

        setup: function(geometry, model_name, comment, material_data) {

            this.data = geometry;
            this.model_name = model_name;
            this.comment = comment;
            this.materialData = material_data;

            // setup geometry
            this.data.num_vertices = geometry.index.count;
            if (geometry.attributes.index_quad) {
                this.data.num_vertices += geometry.attributes.index_quad.count * 3/2;
            };

            var position = new THREE.BufferAttribute(
                new Float32Array(3 * this.data.num_vertices),
                3
            );

            for ( var i = 0; i < geometry.index.count; i++ ) {
                position.copyAt(i, geometry.attributes.position, geometry.index.array[i]);
            };

            if (geometry.attributes.index_quad) {
                var offset = geometry.index.count;
                for ( var i = 0; i < geometry.attributes.index_quad.count; i += 4 ) {
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 0]);
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 1]);
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 2]);
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 0]);
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 2]);
                    position.copyAt(offset++, geometry.attributes.position, geometry.attributes.index_quad.array[i + 3]);
                };
            };

            var bufferGeo = new THREE.BufferGeometry();
            bufferGeo.addAttribute('position', position);
            bufferGeo.computeBoundingSphere();
            bufferGeo.computeVertexNormals();
            bufferGeo.normalizeNormals();

            // setup material
            var materialMesh = new THREE.ShaderMaterial({
                uniforms: THREE.UniformsUtils.merge([
                    THREE.UniformsLib['lights'],
                    {
                        diffuseColor: {type: "c", value: new THREE.Color( 0xeeeeee )},
                        EDGES_MODE: {type: "f", value: null},
                        NODES: {type: "f", value: null},
                        TRANSPART: {type: "f", value: null},
                        LIGHTS: {type: "f", value: null},
                        RESULT: {type: "i", value: null},

                        minContourValue: {type: "f"},
                        maxContourValue: {type: "f"},
                        delta: {type: "f"},
                        minColor: {type: "c"},
                        maxColor: {type: "c"},
                        colorMap: {type: "t"},

                        viewcutX: {type: "f", value: null},
                        viewcutY: {type: "f", value: null},
                        viewcutZ: {type: "f", value: null},
                        Xrev: {type: "f", value: null},
                        Yrev: {type: "f", value: null},
                        Zrev: {type: "f", value: null}
                    }
                ]),
                vertexShader: require("text!./shaders/colorMap.vert"),
                fragmentShader: require("text!./shaders/colorMap.frag"),
                side: THREE.DoubleSide,
                blending: THREE.NoBlending,          
                lights: true
            });
            materialMesh.extensions.derivatives = true;

            this.mesh = new THREE.Mesh( bufferGeo, materialMesh );

            // setup model attributes
            this.set(this._defaults);
            this.setupBarycentric();

            //find min and max of coordinates
            this.attributes.maxX=-1000000000;
            this.attributes.maxY=-1000000000;
            this.attributes.maxZ=-1000000000;
            this.attributes.minX=1000000000;
            this.attributes.minY=1000000000;
            this.attributes.minZ=1000000000;

            for ( var i = 0; i < position.array.length/3; i++) {
                if(position.array[3*i]>this.attributes.maxX) {
                    this.attributes.maxX=position.array[3*i];
                }
                if(position.array[3*i+1]>this.attributes.maxY) {
                    this.attributes.maxY=position.array[3*i+1];
                }
                if(position.array[3*i+2]>this.attributes.maxZ) {
                    this.attributes.maxZ=position.array[3*i+2];
                }
                if(position.array[3*i]<=this.attributes.minX) {
                    this.attributes.minX=position.array[3*i];
                }
                if(position.array[3*i+1]<=this.attributes.minY) {
                    this.attributes.minY=position.array[3*i+1];
                }
                if(position.array[3*i+2]<=this.attributes.minZ) {
                    this.attributes.minZ=position.array[3*i+2];
                }
            };

            this.attributes.viewcutX=this.attributes.maxX;
            this.attributes.viewcutY=this.attributes.maxY;
            this.attributes.viewcutZ=this.attributes.maxZ;
            this.setViewcuts(this.attributes.maxX,this.attributes.maxY,this.attributes.maxZ);
            this.setViewcutRevs(false,false,false);

            // setup nodes
            var geometryNodes = new THREE.BufferGeometry();
            geometryNodes.addAttribute('position', geometry.attributes.position);
            geometryNodes.addAttribute('nodenum', geometry.attributes.nodenum);
            var materialNodes = new THREE.PointsMaterial();

            this.nodes = new THREE.Points( geometryNodes, materialNodes );
        },

        setupBarycentric: function () {
            if ( this.mesh.geometry.getAttribute('barycentric') ) return;

            var BC = [ // barycentric coordinates values at vertices of triangle
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]
            ];

            var barycentric = new THREE.BufferAttribute(
                new Float32Array(3 * this.data.num_vertices),
                3
            );

            for ( var i = 0; i < barycentric.count; i++) {
                barycentric.array[3*i + 0] = BC[i % 3][0];
                barycentric.array[3*i + 1] = BC[i % 3][1];
                barycentric.array[3*i + 2] = BC[i % 3][2];
            };

            if (this.data.attributes.index_quad) {
                var offset = this.data.index.count;
                for ( var i = 0; i < this.data.attributes.index_quad.count; i += 4 ) {
                    offset++;
                    barycentric.setY(offset++, 0);
                    offset++;
                    offset++;
                    offset++;
                    barycentric.setZ(offset++, 0);
                };
            };

            this.mesh.geometry.addAttribute('barycentric', barycentric);

            this.mesh.material.defines.BARYCENTRIC = '';
            this.mesh.material.needsUpdate = true;
        },

        /**
         * Set colormap texture and pass it as uniform to shaders
         * @param {THREE.Color[]} colorMap
         */
        setColorMap: function (colorMap) {
            var texture = [];
            for (var i = 0; i < colorMap.length; i++) {
                texture = texture.concat( colorMap[i].toArray() );
            };

            this.mesh.material.uniforms.colorMap = {
                type: "t",
                value: new THREE.DataTexture(
                    new Float32Array(texture),
                    colorMap.length,
                    1,
                    THREE.RGBFormat,
                    THREE.FloatType
                )
            };
            this.mesh.material.uniforms.colorMap.value.needsUpdate = true;
            this.mesh.material.needsUpdate = true;

            this.mesh.material.uniforms.delta.value = 1./colorMap.length;
        },

        setMinContourValue: function (value) {
            var attrName = this.get("result");
            if (!attrName) return;

            this.mesh.material.uniforms.minContourValue.value = value ||
                this.data.getAttribute(attrName).array[ this.get("minResultVertex") ];
        },

        setMaxContourValue: function (value) {
            var attrName = this.get("result");
            if (!attrName) return;

            this.mesh.material.uniforms.maxContourValue.value = value ||
                this.data.getAttribute(attrName).array[ this.get("maxResultVertex") ];
        },

        setMinColor: function (minColor) {
            if (minColor) {
                this.mesh.material.uniforms.minColor.value = minColor;
                this.mesh.material.defines.USE_MIN_COLOR = '';
            } else {
                delete this.mesh.material.defines.USE_MIN_COLOR;
            }
            this.mesh.material.needsUpdate = true;
        },

        setMaxColor: function (maxColor) {
            if (maxColor) {
                this.mesh.material.uniforms.maxColor.value = maxColor;
                this.mesh.material.defines.USE_MAX_COLOR = '';
            } else {
                delete this.mesh.material.defines.USE_MAX_COLOR;
            }
            this.mesh.material.needsUpdate = true;
        },

        showLights: function (lights) {
            if (lights) {
                this.mesh.material.uniforms.LIGHTS.value = 1;
            } else {
                this.mesh.material.uniforms.LIGHTS.value = 0;
            };
            this.mesh.material.needsUpdate = true;
        },



        setViewcuts: function (viewcutX,viewcutY,viewcutZ) {
            this.mesh.material.uniforms.viewcutX.value=viewcutX;
            this.mesh.material.uniforms.viewcutY.value=viewcutY;
            this.mesh.material.uniforms.viewcutZ.value=viewcutZ;
            this.mesh.material.needsUpdate = true;
        },

        setViewcutRevs: function (Xrev,Yrev,Zrev) {
            this.mesh.material.uniforms.Xrev.value=Xrev;
            this.mesh.material.uniforms.Yrev.value=Yrev;
            this.mesh.material.uniforms.Zrev.value=Zrev;
            this.mesh.material.needsUpdate = true;
        },

        showEdges: function (edges) {
            this.mesh.material.uniforms.EDGES_MODE.value = EDGES_MODES[edges];
            this.mesh.material.needsUpdate = true;
        },
        
        showTransparentPart: function (transparentPart) {
            if (transparentPart) {
                this.mesh.material.uniforms.TRANSPART.value = 1;
                //this.mesh.material.side = THREE.FrontSide;
            } else {
                this.mesh.material.uniforms.TRANSPART.value = 0;
                //this.mesh.material.side = THREE.DoubleSide;
            };
            this.mesh.material.needsUpdate = true;
        },

        showNodes: function (nodes) {
            if (nodes) {
                this.mesh.material.uniforms.NODES.value = 1;
            } else {
                this.mesh.material.uniforms.NODES.value = 0;
            };
            this.mesh.material.needsUpdate = true;
        },

        setResult: function (attrName) {
            if (attrName) {
                var array = this.data.getAttribute(attrName).array;

                // setup 'value' attribute passed to vertex shader
                var value = new THREE.BufferAttribute(
                    new Float32Array(this.data.num_vertices),
                    1
                );
                for ( var i = 0; i < this.data.index.count; i++) {
                    value.array[i] = this.data.getAttribute(attrName).array[ this.data.index.array[i] ];
                };

                if (this.data.attributes.index_quad) {
                    var offset = this.data.index.count;
                    for ( var i = 0; i < this.data.attributes.index_quad.count; i += 4 ) {
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 0]);
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 1]);
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 2]);
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 0]);
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 2]);
                        value.copyAt(offset++, this.data.getAttribute(attrName),  this.data.attributes.index_quad.array[i + 3]);
                    };
                };

                this.mesh.geometry.addAttribute('value', value);

                this.mesh.material.uniforms.RESULT.value = 1;

                this.findMinResultVertex();
                this.findMaxResultVertex();
            } else {
                this.mesh.geometry.removeAttribute('value');
                this.mesh.material.uniforms.RESULT.value = 0;
            };
            this.mesh.material.needsUpdate = true;
        },

        findMinResultVertex: function () {
            var attrName = this.get("result");
            var array = this.data.getAttribute(attrName).array;

            // find minimum vertex
            var vertex = 0;
            for (var i = 1; i < array.length; i++) {
                if (array[i] < array[vertex]) {
                    vertex = i;
                }
            };

            this.set('minResultVertex', vertex);
            this.set("minContourValue",array[vertex]);
        },

        findMaxResultVertex: function () {
            var attrName = this.get("result");
            var array = this.data.getAttribute(attrName).array;

            // find maximum vertex
            var vertex = 0;
            for (var i = 1; i < array.length; i++) {
                if (array[i] > array[vertex]) {
                    vertex = i;
                }
            };

            this.set('maxResultVertex', vertex);
            this.set("maxContourValue",array[vertex]);
        },

        queryNode: function(vIndex) {

            var resultList=[];

            if(typeof(this.get('result')) !== 'undefined') {
                Object.keys(this.data.attributes).forEach(function(key) {
                    if ((key!='position')&&(key!='nodenum')&&(key!='index_quad')) {
                        var entry = {};
                        entry.name = key;
                        entry.value = this.data.attributes[key].array[vIndex].toExponential(3),

                        resultList.push(entry);
                    }
                }, this);

            }

            return {
                nodenum: this.data.attributes.nodenum.array[vIndex],
                resultList: resultList
            };
        }

    });

    return Model;

});
