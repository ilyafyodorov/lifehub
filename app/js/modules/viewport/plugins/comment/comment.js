define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./comment"),

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
        },

        render: function () {
            
            if(this.model.comment === undefined) {
                this.model.comment = 'Comment not available'
            }
            
            if(this.model.model_name === undefined) {
                this.model.model_name = 'Model name not available'
            }
            
            this.$el.html(this.template({
                comment: this.model.comment,
                model_name: this.model.model_name
                }));
            return this;
        },

    });

    return View;
});