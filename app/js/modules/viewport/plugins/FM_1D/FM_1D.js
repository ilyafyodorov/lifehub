define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./FM_1D"),

        events: {
            'click .btn-primary': 'calcFM'
        },

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
            this.listenTo(this.model, "change:selectedNode", this.render);
        },

        render: function () {

            var text;
            var queryData;

            if(this.model.attributes.selectedNode === undefined) {
                text = 'Node not selected'
                this.$el.html(this.template({
                    text: text,
                    fullView: 0.0
                }));
            } else {

                queryData = this.model.queryNode(this.model.attributes.selectedNode);
                text = 'Node '+queryData.nodenum.toString()+' selected';
                this.$el.html(this.template({
                    text: text,
                    fullView: 1.0,
                    resultList: queryData.resultList
                }));
            }

            return this;

        },

        // This function calculates FM lifetime at node with 1D approach (Paris law and K=1.12*S*sqrt(pi*a))

        calcFM: function () {

            var queryData;

            if(this.model.attributes.selectedNode !== undefined) {

                //get all parameters for the selected node
                queryData = this.model.queryNode(this.model.attributes.selectedNode);

                //get user-defined selections

                var selTDAM = $('#FM1Dplugin-selTDAM');
                var selSIGR = $('#FM1Dplugin-selSIGR');
                var selLCF = $('#FM1Dplugin-selLCF');

                var TDAMValue = parseFloat(queryData.resultList[selTDAM[0].selectedIndex].value);
                var SIGRValue = parseFloat(queryData.resultList[selSIGR[0].selectedIndex].value)/1000000;
                //LCF 3D data field is optional
                var LCFValue = undefined;
                if (selLCF[0].selectedIndex!==0) {
                    LCFValue = parseFloat(queryData.resultList[selLCF[0].selectedIndex-1].value);
                }

                var initA = parseFloat($('#FM1Dplugin-initA').val());
                if(isNaN(initA)) {
                    initA = 0.5;
                }

                //check for relevant material data availability
                if(this.model.materialData.FM) {

                    //scan temperatures, find left side level
                    var leftSideIndex = 0;
                    var i;
                    for (i = 0; i < this.model.materialData.FM.Temp.length-1; i++) {
                        if((TDAMValue)>this.model.materialData.FM.Temp[i]) {
                            leftSideIndex=i;
                        }
                    }
                    var TempLeft = this.model.materialData.FM.Temp[leftSideIndex];
                    var TempRight = this.model.materialData.FM.Temp[leftSideIndex+1];

                    //get KIC for given temperature
                    var K1CLeft = this.model.materialData.FM.K1C[leftSideIndex];
                    var K1CRight = this.model.materialData.FM.K1C[leftSideIndex+1];
                    var K1C = K1CLeft+(K1CRight-K1CLeft)/(TempRight-TempLeft)*(TDAMValue-TempLeft);

                    //get C for given temperature
                    var CLeft = this.model.materialData.FM.C[leftSideIndex];
                    var CRight = this.model.materialData.FM.C[leftSideIndex+1];
                    var CVal = CLeft+(CRight-CLeft)/(TempRight-TempLeft)*(TDAMValue-TempLeft);

                    //get m for given temperature
                    var mLeft = this.model.materialData.FM.m[leftSideIndex];
                    var mRight = this.model.materialData.FM.m[leftSideIndex+1];
                    var mVal = mLeft+(mRight-mLeft)/(TempRight-TempLeft)*(TDAMValue-TempLeft);
                    
                    var aCrit = K1C*K1C*1000/(1.12*1.12*SIGRValue*SIGRValue*Math.PI);
                    
                    var NFMNumerator = 2*(Math.pow(aCrit/1000,(2-mVal)/2)-Math.pow(initA/1000,(2-mVal)/2));
                    var NFMDenominator = (2-mVal)*CVal*Math.pow(SIGRValue*1.12*Math.sqrt(Math.PI),mVal);
                    var NFM = Math.floor(NFMNumerator/NFMDenominator);

                    $('#FM1Dplugin-aCrit')[0].innerHTML = aCrit;
                    $('#FM1Dplugin-FMLife')[0].innerHTML = NFM;
                    if(LCFValue!==undefined)
                    {
                        $('#FM1Dplugin-sensResult')[0].innerHTML = NFM + LCFValue;
                    }

                } else {

                    $('#FM1Dplugin-aCrit')[0].innerHTML = "no material data attached to this results file";
                    $('#FM1Dplugin-FMLife')[0].innerHTML = "no material data attached to this results file";
                    $('#FM1Dplugin-sensResult')[0].innerHTML = "no material data attached to this results file";

                }

            }


        }

    });

    return View;
});
