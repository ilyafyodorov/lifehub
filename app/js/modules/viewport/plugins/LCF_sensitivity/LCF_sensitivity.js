define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./LCF_sensitivity"),

        events: {
            'click .btn-primary': 'calcLCF'
        },

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
            this.listenTo(this.model, "change:selectedNode", this.render);
        },

        render: function () {

            var text;
            var queryData;

            if(this.model.attributes.selectedNode === undefined) {
                text = 'Node not selected'
                this.$el.html(this.template({
                    text: text,
                    fullView: 0.0
                }));
            } else {

                queryData = this.model.queryNode(this.model.attributes.selectedNode);
                text = 'Node '+queryData.nodenum.toString()+' selected';
                this.$el.html(this.template({
                    text: text,
                    fullView: 1.0,
                    resultList: queryData.resultList
                }));
            }

            return this;

        },

        // This function calculates LCF lifetime based on temperature, stress, temperature delta, notch factor.
        // Calculation is performed by simple interpolation/ extrapolation of material data (MTX format).
        // If 3D LCF lifetime results are also available, 1D assessment result is adjusted to match 3D result
        // for zero temperature delta and notch = 1.0

        calcLCF: function () {

            var queryData;

            if(this.model.attributes.selectedNode !== undefined) {

                //get all parameters for the selected node
                queryData = this.model.queryNode(this.model.attributes.selectedNode);

                //get user-defined selections

                var selTDAM = $('#LCFplugin-selTDAM');
                var selSIGR = $('#LCFplugin-selSIGR');
                var selLCF = $('#LCFplugin-selLCF');

                var TDAMValue = parseFloat(queryData.resultList[selTDAM[0].selectedIndex].value);
                var SIGRValue = parseFloat(queryData.resultList[selSIGR[0].selectedIndex].value);
                //LCF 3D data field is optional
                var LCFValue = undefined;
                if (selLCF[0].selectedIndex!==0) {
                    LCFValue = parseFloat(queryData.resultList[selLCF[0].selectedIndex-1].value);
                }

                var TDeltaValue = parseFloat($('#LCFplugin-deltaT').val());
                if(isNaN(TDeltaValue)) {
                    TDeltaValue = 0.0;
                }

                var notchFValue = parseFloat($('#LCFplugin-notchF').val());
                if(isNaN(notchFValue)) {
                    notchFValue = 1.0;
                }

                //check for relevant material data availability
                if(this.model.materialData.LCF) {

                    //calculate LCF life from (stress*notch), (temperature+TDelta) and material data
                    //scan temperatures, find left side level
                    var leftSideIndex = 0;
                    var i;
                    for (i = 0; i < this.model.materialData.LCF.Temp.length-1; i++) {
                        if((TDAMValue+TDeltaValue)>this.model.materialData.LCF.Temp[i]) {
                            leftSideIndex=i;
                        }
                    }
                    var TempLeft = this.model.materialData.LCF.Temp[leftSideIndex];
                    var TempRight = this.model.materialData.LCF.Temp[leftSideIndex+1];
                    var StressLeft = this.model.materialData.LCF.Stress.slice(leftSideIndex*this.model.materialData.LCF.LCF.length,(leftSideIndex+1)*this.model.materialData.LCF.LCF.length);
                    var StressRight = this.model.materialData.LCF.Stress.slice((leftSideIndex+1)*this.model.materialData.LCF.LCF.length,(leftSideIndex+2)*this.model.materialData.LCF.LCF.length);

                    //make array of lcf vs stress at given temperature
                    var StressAtTemp = [];

                    for (i = 0; i < this.model.materialData.LCF.LCF.length; i++) {
                        StressAtTemp.push(StressLeft[i]+(StressRight[i]-StressLeft[i])/(TempRight-TempLeft)*(TDAMValue+TDeltaValue-TempLeft))
                    }

                    //scan stresses, find left side level
                    leftSideIndex=0;
                    for (i = 0; i < StressAtTemp.length-1; i++) {
                        if(SIGRValue*notchFValue<StressAtTemp[i]) {
                            leftSideIndex=i;
                        }
                    }

                    //calculate lifetime
                    var StressLeft = StressAtTemp[leftSideIndex];
                    var StressRight = StressAtTemp[leftSideIndex+1];

                    var LCFLeft = this.model.materialData.LCF.LCF[leftSideIndex];
                    var LCFRight = this.model.materialData.LCF.LCF[leftSideIndex+1];

                    var logLCF = Math.log10(LCFLeft) + (Math.log10(LCFRight)-Math.log10(LCFLeft))/(StressRight-StressLeft)*(SIGRValue*notchFValue-StressLeft);

                    var LCFresult = Math.pow(10,logLCF);

                    //if 3D LCF result available

                    if(LCFValue!==undefined)
                    {
                        //calculate calculate LCF life from (stress), (temperature) and material data
                        leftSideIndex = 0;
                        for (i = 0; i < this.model.materialData.LCF.Temp.length-1; i++) {
                            if((TDAMValue)>this.model.materialData.LCF.Temp[i]) {
                                leftSideIndex=i;
                            }
                        }
                        TempLeft = this.model.materialData.LCF.Temp[leftSideIndex];
                        TempRight = this.model.materialData.LCF.Temp[leftSideIndex+1];
                        StressLeft = this.model.materialData.LCF.Stress.slice(leftSideIndex*this.model.materialData.LCF.LCF.length,(leftSideIndex+1)*this.model.materialData.LCF.LCF.length);
                        StressRight = this.model.materialData.LCF.Stress.slice((leftSideIndex+1)*this.model.materialData.LCF.LCF.length,(leftSideIndex+2)*this.model.materialData.LCF.LCF.length);

                        //make array of lcf vs stress at given temperature
                        StressAtTemp = [];

                        for (i = 0; i < this.model.materialData.LCF.LCF.length; i++) {
                            StressAtTemp.push(StressLeft[i]+(StressRight[i]-StressLeft[i])/(TempRight-TempLeft)*(TDAMValue-TempLeft))
                        }

                        //scan stresses, find left side level
                        leftSideIndex=0;
                        for (i = 0; i < StressAtTemp.length-1; i++) {
                            if(SIGRValue<StressAtTemp[i]) {
                                leftSideIndex=i;
                            }
                        }

                        //calculate lifetime
                        StressLeft = StressAtTemp[leftSideIndex];
                        StressRight = StressAtTemp[leftSideIndex+1];

                        LCFLeft = this.model.materialData.LCF.LCF[leftSideIndex];
                        LCFRight = this.model.materialData.LCF.LCF[leftSideIndex+1];

                        logLCF = Math.log10(LCFLeft) + (Math.log10(LCFRight)-Math.log10(LCFLeft))/(StressRight-StressLeft)*(SIGRValue-StressLeft);

                        var BaseLCFresult = Math.pow(10,logLCF);

                        //find LCF delta

                        var LCFdelta = LCFresult/BaseLCFresult;

                        //add to 3D LCF result

                        LCFresult = LCFValue*LCFdelta;
                    }


                    $('#LCFplugin-sensResult')[0].innerHTML = Math.round(LCFresult);

                } else {

                    $('#LCFplugin-sensResult')[0].innerHTML = "no material data attached to this results file";

                }

            }


        }

    });

    return View;
});
