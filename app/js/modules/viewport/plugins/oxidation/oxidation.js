define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./oxidation"),

        events: {
            'click .btn-primary': 'calcOX'
        },

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
            this.listenTo(this.model, "change:selectedNode", this.render);
        },

        render: function () {

            var text;
            var queryData;

            if(this.model.attributes.selectedNode === undefined) {
                text = 'Node not selected'
                this.$el.html(this.template({
                    text: text,
                    fullView: 0.0
                }));
            } else {

                queryData = this.model.queryNode(this.model.attributes.selectedNode);
                text = 'Node '+queryData.nodenum.toString()+' selected';
                this.$el.html(this.template({
                    text: text,
                    fullView: 1.0,
                    resultList: queryData.resultList
                }));
            }

            return this;

        },

        // This function calculates Oxidation thickness based on metal temperature.

        calcOX: function () {

            var queryData;

            if(this.model.attributes.selectedNode !== undefined) {

                //get all parameters for the selected node
                queryData = this.model.queryNode(this.model.attributes.selectedNode);

                //get user-defined selections

                var selTDAM = $('#OXplugin-selTDAM');
                var TDAMValue = parseFloat(queryData.resultList[selTDAM[0].selectedIndex].value);

                var LtValue = parseFloat($('#OXplugin-reqLT').val());
                if(isNaN(LtValue)) {
                    LtValue = 24000;
                }

                var DTValue = parseFloat($('#OXplugin-deltaT').val());
                if(isNaN(DTValue)) {
                    DTValue = 0.0;
                }

                //check for relevant material data availability
                if(this.model.materialData.Oxid) {
                   
                    //calculate temperature+TDelta
                    //scan temperatures, find left side level
                    
                    var leftSideIndex = 0;
                    var i;
                    for (i = 0; i < this.model.materialData.Oxid.Temp.length-1; i++) {
                        if((TDAMValue+DTValue)>this.model.materialData.Oxid.Temp[i]) {
                            leftSideIndex=i;
                        }
                    }
                    var TempLeft = this.model.materialData.Oxid.Temp[leftSideIndex];
                    var TempRight = this.model.materialData.Oxid.Temp[leftSideIndex+1];
                    var amaLeft = this.model.materialData.Oxid.ama.slice(leftSideIndex*this.model.materialData.Oxid.Time.length,(leftSideIndex+1)*this.model.materialData.Oxid.Time.length);
                    var amaRight = this.model.materialData.Oxid.ama.slice((leftSideIndex+1)*this.model.materialData.Oxid.Time.length,(leftSideIndex+2)*this.model.materialData.Oxid.Time.length);

                    //make array of time vs ama at given temperature
                    var amaAtTemp = [];
                    
                    for (i = 0; i < this.model.materialData.Oxid.Time.length; i++) {
                        amaAtTemp.push(amaLeft[i]+(amaRight[i]-amaLeft[i])/(TempRight-TempLeft)*(TDAMValue+DTValue-TempLeft))
                    }

                    //scan time, find left side level
                    leftSideIndex=0;
                    for (i = 0; i < this.model.materialData.Oxid.Time.length-1; i++) {
                        if(LtValue>this.model.materialData.Oxid.Time[i]) {
                            leftSideIndex=i;
                        }
                    }

                    //calculate lifetime
                    var amaLeft = amaAtTemp[leftSideIndex];
                    var amaRight = amaAtTemp[leftSideIndex+1];
                    
                    var timeLeft = this.model.materialData.Oxid.Time[leftSideIndex]
                    var timeRight = this.model.materialData.Oxid.Time[leftSideIndex+1]

                    var OXResult = amaLeft + (amaRight-amaLeft)/(timeRight-timeLeft)*(LtValue-timeLeft)
                    
                    $('#OXplugin-sensResult')[0].innerHTML = Math.round(OXResult);

                } else {

                    $('#OXplugin-sensResult')[0].innerHTML = "no material data attached to this results file";

                }

            }

        }

    });

    return View;
});
