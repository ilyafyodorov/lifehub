define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./Damage_risk"),

        events: {
            'click .btn-primary': 'calcDamageRisk'
        },

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
        },
        
        render: function () {

            var text;
           
            //define list of results

            var resultList=[];

            if(typeof(this.model.get('result')) !== 'undefined') {
                Object.keys(this.model.data.attributes).forEach(function(key) {
                    if ((key!='position')&&(key!='nodenum')&&(key!='index_quad')) {
                        var entry = {};
                        entry.name = key;

                        resultList.push(entry);
                    }
                }, this);            
            }
            
            this.$el.html(this.template({resultList:resultList}));

            return this;

        },
        
        erf: function(x) {
            
            var t = 1/(1+0.3275911*Math.abs(x));

            var a1 =  0.254829592;
            var a2 = -0.284496736;
            var a3 =  1.421413741;
            var a4 = -1.453152027;
            var a5 =  1.061405429;

            var erf = 1-(((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*Math.exp(-x*x);
            var sign = 1;
            
            if(x<0) {
                sign = -1;
            }

            return erf*sign;
            
        },

        calcDamageRisk: function () {
            
            var missingInput = 0;
            
            var NCycl = parseFloat($('#DamageRiskplugin-NCycl').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }
            
            var lole1VAlue = parseFloat($('#DamageRiskplugin-lole1VAlue').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }            
            var lole2VAlue = parseFloat($('#DamageRiskplugin-lole2VAlue').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            var lole3VAlue = parseFloat($('#DamageRiskplugin-lole3VAlue').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            
            var OperData_lole1Val = parseFloat($('#DamageRiskplugin-OperData-lole1Val').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            var OperData_lole1Dur = parseFloat($('#DamageRiskplugin-OperData-lole1Dur').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            
            var OperData_lole2Val = parseFloat($('#DamageRiskplugin-OperData-lole2Val').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            var OperData_lole2Dur = parseFloat($('#DamageRiskplugin-OperData-lole2Dur').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
                        
            var OperData_lole3Val = parseFloat($('#DamageRiskplugin-OperData-lole3Val').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            var OperData_lole3Dur = parseFloat($('#DamageRiskplugin-OperData-lole3Dur').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }             
            
            if(missingInput === 0) {
                
                $('#DamageRiskplugin-InpStat')[0].innerHTML = "Input OK";
                
                var MinLCFKey = $('#DamageRiskplugin-selMinLCF').val();
                var MeanLCFKey = $('#DamageRiskplugin-selMeanLCF').val();
                
                var totalCycles = OperData_lole1Dur + OperData_lole2Dur + OperData_lole3Dur;
                var avLL = (OperData_lole1Dur*OperData_lole1Val + OperData_lole2Dur*OperData_lole2Val + OperData_lole3Dur*OperData_lole3Val)/totalCycles;
                
                var flcf1;
                var flcf2;
                var flcf3;
                
                if ((avLL>=lole1VAlue)&&(avLL<lole2VAlue)) {
                    flcf1 = 1-Math.abs((avLL-lole1VAlue)/(lole2VAlue-lole1VAlue));
                    flcf2 = 1-Math.abs((avLL-lole2VAlue)/(lole2VAlue-lole1VAlue));
                    flcf3 = 0;
                }
                if ((avLL>=lole2VAlue)&&(avLL<=lole3VAlue)) {
                    flcf1 = 0;
                    flcf2 = 1-Math.abs((avLL-lole2VAlue)/(lole3VAlue-lole2VAlue));
                    flcf3 = 1-Math.abs((avLL-lole3VAlue)/(lole3VAlue-lole2VAlue));
                }

                if (typeof(this.model.data.attributes['Damage probability']) == 'undefined') {
                    this.model.data.attributes['Damage probability'] = new THREE.BufferAttribute(
                        new Float32Array(this.model.data.attributes[MinLCFKey].array.length),
                        1
                    );
                }
                
                
                var DamPro = this.model.data.attributes['Damage probability'].array;

                for(var j=0;j<this.model.data.attributes[MinLCFKey].array.length;j++) {
                    DamPro[j]=1;
                }

                //Calculate damage probability for each node

                for (var i=0;i<DamPro.length;i++) {
                
                    var vlcf100 = this.model.data.attributes[MeanLCFKey].array[i];
                    
                    var vlcf1Key = $('#DamageRiskplugin-lole1LCF').val();
                    var vlcf1 = this.model.data.attributes[vlcf1Key].array[i];
                    var vlcf2Key = $('#DamageRiskplugin-lole2LCF').val();
                    var vlcf2 = this.model.data.attributes[vlcf2Key].array[i];                
                    var vlcf3Key = $('#DamageRiskplugin-lole3LCF').val();
                    var vlcf3 = this.model.data.attributes[vlcf3Key].array[i];
                    
                    //operation data influence factor
                    var loadFac = (vlcf1*flcf1+vlcf2*flcf2+vlcf3*flcf3)/(vlcf100);


                    var sigma = (this.model.data.attributes[MeanLCFKey].array[i]-this.model.data.attributes[MinLCFKey].array[i])/3;
                    var mean = this.model.data.attributes[MeanLCFKey].array[i];
                    DamPro[i]=1/2*(1+this.erf(((NCycl+loadFac*totalCycles)-mean)/(sigma*1.414213562)));
                }

                this.model.set('result', MinLCFKey);
                this.model.set('result', 'Damage probability');

                $('#DamageRiskModal').modal('hide');
                
            } else {
                $('#DamageRiskplugin-InpStat')[0].innerHTML = "Some input data is missing";                
            }

        }

    });

    return View;
});
