define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var radio = require("radio").channel("app");

    var View = Backbone.View.extend({

        template: require("hbs!./Remaining_life"),

        events: {
            'click .btn-primary': 'calcRemLife'
        },

        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
        },

        render: function () {

            var text;
           
            //define list of results

            var resultList=[];

            if(typeof(this.model.get('result')) !== 'undefined') {
                Object.keys(this.model.data.attributes).forEach(function(key) {
                    if ((key!='position')&&(key!='nodenum')&&(key!='index_quad')) {
                        var entry = {};
                        entry.name = key;

                        resultList.push(entry);
                    }
                }, this);            
            }
            
            this.$el.html(this.template({resultList:resultList}));

            return this;

        },

        calcRemLife: function () {
            
            var missingInput = 0;
            
            var NCycl = parseFloat($('#RemLifeplugin-NCycl').val());
            if(isNaN(NCycl)) {
                missingInput+=1;
            }
            var NOH = parseFloat($('#RemLifeplugin-NOH').val());
            if(isNaN(NOH)) {
                missingInput+=1;
            }     
            var COH = parseFloat($('#RemLifeplugin-COH').val());
            if(isNaN(COH)) {
                COH=20;
            }
            var NWarm = parseFloat($('#RemLifeplugin-NWarm').val());
            if(isNaN(NWarm)) {
                missingInput+=1;
            }  
            var CWarm = parseFloat($('#RemLifeplugin-CWarm').val());
            if(isNaN(CWarm)) {
                CWarm=0.75;
            }           
            var NTrip = parseFloat($('#RemLifeplugin-NTrip').val());
            if(isNaN(NTrip)) {
                missingInput+=1;
            }  
            var CTrip = parseFloat($('#RemLifeplugin-CTrip').val());
            if(isNaN(CTrip)) {
                CTrip=10;
            }            
            
            if(missingInput === 0) {
                
                
                $('#RemLifeplugin-InpStat')[0].innerHTML = "Input OK";
                
                var LCFKey = $('#RemLifeplugin-selLCF').val();
                
                this.model.data.attributes['Remaining lifetime, %'] = new THREE.BufferAttribute(
                    new Float32Array(this.model.data.attributes[LCFKey].array.length),
                    1
                );
                
                var RemLt = this.model.data.attributes['Remaining lifetime, %'].array;

                for(var j=0;j<this.model.data.attributes[LCFKey].array.length;j++) {
                    RemLt[j]=this.model.data.attributes[LCFKey].array[j];
                }

                var i;                
                var nCold;
                var realStarts;
                //Calculate remaining lifetime for each node
                for (i=0;i<RemLt.length;i++) {
                    nCold = NCycl - NWarm - NTrip;
                    realStarts = nCold + NWarm*CWarm + NTrip*CTrip + NOH/COH;
                    RemLt[i] = Math.round((1-realStarts/RemLt[i])*100);
                    if (RemLt[i]<0) {
                        RemLt[i]=0;
                    }
                }
                this.model.set('result', 'Remaining lifetime, %');
                $('#RemLifeModal').modal('hide');
                
            } else {
                $('#RemLifeplugin-InpStat')[0].innerHTML = "Some operation data is missing";                
            }
            

        }

    });

    return View;
});
