define( function (require) {
    "use strict";

    var Backbone = require("backbone");
    var THREE = require("three");

    var MAX_TICKS_OFFSET = 10;
    var MAX_FONT_SIZE = 14;
    var FONT = 'px sans-serif';

    var View = Backbone.View.extend({

        className: "legend",

        template: '<canvas></canvas>',

        events: {
            'click': function (e) {
                e.stopPropagation();
                this.$el.toggleClass('legend-minimized');
                this.render();
            }
        },

        initialize: function () {
            this.$el.html(this.template);

            this.ctx = this.$('canvas')[0].getContext('2d');
            this.colorMap = [];
            this.ticks = [];
            this.title = '';
            this.pars = {};

            this.listenTo(
                this.model,
                "change:result change:minContourValue change:maxContourValue change:colorMap change:minColor change:maxColor",
                this.render
            );
        },

        render: function () {
            if ( this.model.get('result') ) {
                this.prepareLegend().calcLegend().setSize().drawLegend().$el.show();
            } else {
                this.$el.hide();
            };

            return this;
        },

        /**
         * Prepares this.colorMap, this.ticks and this.title before drawing the legend
         * Takes care of min/max colors and ticks if necessary
         */
        prepareLegend: function () {
            this.colorMap = this.model.get("colorMap").slice();

            var minResultValue = this.model.data.getAttribute( this.model.get('result') ).array[ this.model.get('minResultVertex') ];
            var maxResultValue = this.model.data.getAttribute( this.model.get('result') ).array[ this.model.get('maxResultVertex') ];

            var minContourValue = this.model.get("minContourValue") || minResultValue;
            var maxContourValue = this.model.get("maxContourValue") || maxResultValue;

            this.ticks = [];
            var step = (maxContourValue - minContourValue) / this.colorMap.length;
            for ( var i = 0; i < this.colorMap.length + 1; i++ ) {
                this.ticks.push( + minContourValue + i*step );
            };

            if (minContourValue > minResultValue) {
                this.colorMap.unshift( this.model.get('minColor') || this.colorMap[0] );
                this.ticks.unshift(minResultValue);
            };
            if (maxContourValue < maxResultValue) {
                this.colorMap.push( this.model.get('maxColor') || this.colorMap[ this.colorMap.length - 1 ] );
                this.ticks.push(maxResultValue);
            };

            // revert arrays since legend is drawn from max to min
            this.colorMap.reverse();
            this.ticks.reverse();

            // set title
            this.title = this.model.get('result');

            return this;
        },

        /**
         * Calculates legend parameters this.pars
         *
         * Legend height consists of (top-to-bottom):
         * padding - for centering
         * 2.5*font_size - for title
         * num_colors*h - for colored rectangles and ticks
         * 0.5*font_size - for min tick
         * padding - for centering
         * plus extra 1px reserved to have half-integer padding of at least 0.5
         *
         * Legend width is max( title_width, w + ticks_offset + ticks_width )
         * plus extra 1px for padding of 0.5
         */
        calcLegend: function () {
            var height = this.$el.height();
            var numColors = this.colorMap.length;

            // calc colored rectangles width and height
            var w = 15;
            var h;
            if (height > (3 + 2*numColors)*MAX_FONT_SIZE + 1) {
                h = (height - 3*MAX_FONT_SIZE - 1) / numColors | 0;
            } else {
                h = (height - 1) / (3/2 + numColors) | 0;
            };

            this.pars.w = w;
            this.pars.h = h;

            // calc padding and font parameters
            var font_size = Math.min( h/2 | 0, MAX_FONT_SIZE );
            var title_height = (2.5*font_size | 0);
            var ticks_offset = Math.min( font_size, MAX_TICKS_OFFSET );
            var padding = ((height - h*numColors - 3*font_size - 1) / 2 | 0) + 0.5 ; // padding is always half-integer to avoid aliasing

            this.pars.font_size = font_size;
            this.pars.title_height = title_height;
            this.pars.ticks_offset = ticks_offset;
            this.pars.padding = padding;

            // calc legend width
            this.ctx.font = font_size + FONT;

            var ticks_width = Math.max( this.ctx.measureText( this.ticks[0].toExponential(3) ).width,
                                        this.ctx.measureText( this.ticks[numColors].toExponential(3) ).width );
            var title_width = this.ctx.measureText( this.title ).width;

            var width = 1 + Math.max( title_width,
                                      w + ticks_offset + ticks_width );

            this.pars.height = height;
            this.pars.width = width;

            return this;
        },

        setSize: function () {
            var pixelRatio = window.devicePixelRatio;

            this.ctx.canvas.height = this.pars.height * pixelRatio;
            this.ctx.canvas.width = this.pars.width * pixelRatio;

            this.ctx.canvas.style.height = this.pars.height + 'px';
            this.ctx.canvas.style.width = this.pars.width + 'px';

            if (pixelRatio != 1) {
                this.pars.w *= pixelRatio;
                this.pars.h *= pixelRatio;
                this.pars.font_size *= pixelRatio;
                this.pars.title_height *= pixelRatio;
                this.pars.ticks_offset *= pixelRatio;
                this.pars.padding *= pixelRatio;
            };

            return this;
        },

        /**
         * Draws legend on canvas using this.colorMap, this.ticks and this.pars
         */
        drawLegend: function () {
            var w = this.pars.w;
            var h = this.pars.h;
            var font_size = this.pars.font_size;
            var title_height = this.pars.title_height;
            var ticks_offset = this.pars.ticks_offset;
            var padding = this.pars.padding;
            var numColors = this.colorMap.length;

            // draw title
            this.ctx.font = font_size + FONT;
            this.ctx.textBaseline = "top";
            this.ctx.fillText( this.title, 0.5, padding);

            // draw color map
            for ( var i = 0; i < numColors; i++ ) {
                this.fillStrokeRect( 0.5, padding + title_height + h*i, w, h,
                                     "#" + this.colorMap[i].getHexString() );
            };

            // draw ticks
            this.ctx.font = font_size + FONT;
            this.ctx.textBaseline = "middle";
            this.ctx.fillStyle = 'black';
            for ( var i = 0; i < numColors + 1; i++ ) {
                this.ctx.fillText( this.ticks[i].toExponential(3),
                                   0.5 + w + ticks_offset,
                                   padding + title_height + h*i );
            };

            return this;
        },

        fillStrokeRect: function (x, y, w, h, color) {
            this.ctx.fillStyle = color;
            this.ctx.fillRect(x, y, w, h);
            this.ctx.strokeRect(x, y, w, h);
        }
    });

    return View;
} );
