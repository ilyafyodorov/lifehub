define(function(require) {
    "use strict";

    var Backbone = require("backbone");
    var _ = require('underscore');
    require("modelbinder");
    var THREE = require("three");
    require('bootstrap_toggle');

    var BLUE_TO_RED = [
        new THREE.Color(0.164, 0.043, 0.850),
        new THREE.Color(0.150, 0.306, 1.000),
        new THREE.Color(0.250, 0.630, 1.000),
        new THREE.Color(0.450, 0.853, 1.000),
        new THREE.Color(0.670, 0.973, 1.000),
        new THREE.Color(0.880, 1.000, 1.000),
        new THREE.Color(1.000, 1.000, 0.750),
        new THREE.Color(1.000, 0.880, 0.600),
        new THREE.Color(1.000, 0.679, 0.450),
        new THREE.Color(0.970, 0.430, 0.370),
        new THREE.Color(0.850, 0.150, 0.196),
        new THREE.Color(0.650, 0.000, 0.130)
    ];
    var RED_TO_BLUE = BLUE_TO_RED.slice();
    RED_TO_BLUE.reverse();

    var View = Backbone.View.extend({

        tagName: "div",
        className: "options",

        template: require("hbs!./options"),

        events: {
            'hide.bs.collapse #options-accordion': 'toggleMenu',
            'show.bs.collapse #options-accordion': 'toggleMenu',
            'click .js-maxColor-colormap': function () {
                var colormap = this.model.get('colorMap');
                this.model.set('maxColor', colormap[colormap.length - 1]);
            },
            'click .js-minColor-colormap': function () {
                var colormap = this.model.get('colorMap');
                this.model.set('minColor', colormap[0]);
            },
            'change .js-colormap': function (e) {
                switch ( $(e.target).val() ) {
                case 'blue-to-red':
                    this.model.set('colorMap', BLUE_TO_RED);
                    break;
                case 'red-to-blue':
                    this.model.set('colorMap', RED_TO_BLUE);
                    break;
                }
            },
            'click .js-findMinMax': function () {
                this.trigger('minMaxNeedsUpdate');
            },
            'change .animatebtn': function () {
                this.model.attributes.animateCheck = 1 - this.model.attributes.animateCheck;
            },
            'change .VRviewbtn': function () {
                this.model.set("VRCheck",1 - this.model.attributes.VRCheck);
            }
        },

        initialize: function () {
            this.binder = new Backbone.ModelBinder();
        },

        remove: function () {
            this.binder.unbind();
            Backbone.View.prototype.remove.apply(this, arguments);
            return this;
        },

        render: function () {
            this.$el.html(this.template());

            var bindings = Backbone.ModelBinder.createDefaultBindings(this.el, 'name');

            var colorConverter = function(direction, value) {
                if (direction === Backbone.ModelBinder.Constants.ModelToView) {
                    return '#' + value.getHexString();
                } else {
                    return new THREE.Color(value);
                }
            };

            bindings['maxColor'].converter = colorConverter;
            bindings['minColor'].converter = colorConverter;

            this.binder.bind(this.model, this.el, bindings);

            this.$('input[type=checkbox][data-toggle^=toggle]').bootstrapToggle();

            return this;
        },

        /**
         * Toggle class and rotate menu arrow when menu section opened/closed
         */
        toggleMenu: function (e) {
            $(e.target)
                .prev('.panel-heading')
                .find('i.indicator')
                .toggleClass('glyphicon-menu-right glyphicon-menu-down');
        },

    });

    return View;
});
