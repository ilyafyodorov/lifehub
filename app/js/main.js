define(function (require) {
    'use strict';

    require('bootstrap');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var radio = require('radio').channel('app');

    var Router = require('./router');
    var Navbar = require('modules/navbar/navbar');
    var fileTree = require('modules/navbar/filetree');
    var Viewport = require('modules/viewport/viewport');
    var Model = require('modules/viewport/model');
    var resultsSelectionView = require('modules/navbar/resultsSelectionView');

    // links to plugins should be added here

    var Comment = require('modules/viewport/plugins/comment/comment');
    var LCFSensitivity = require('modules/viewport/plugins/LCF_sensitivity/LCF_sensitivity');
    var OXSensitivity = require('modules/viewport/plugins/oxidation/oxidation');
    var FM_1D = require('modules/viewport/plugins/FM_1D/FM_1D');
    //var RemLife = require('modules/viewport/plugins/Remaining_lifetime/Remaining_life');
    //var DamageRisk = require('modules/viewport/plugins/damage_risk/Damage_risk');

    app = window.app = {};

    _.extend(app, Backbone.Events, {

        dataRoot: '/data',

        start: function () {
            this.router = new Router();
            this.listenTo(this.router, 'route:browse', this.browse);

            radio.on('navigate', function (url) {
                this.router.navigate(url);
            }, this);

            radio.on('select:model', function (id) {
                if (this.viewport) this.viewport.remove();
                if (this.resultsSelectionView) this.resultsSelectionView.remove();

                var displayModel = new Model({id: id});

                this.viewport = new Viewport({
                    el: $('#js-viewport'),
                    model: displayModel
                });

                this.resultsSelectionView = new resultsSelectionView({
                    model: displayModel
                });

                $('#browse-button').after(this.resultsSelectionView.render().$el);

                //pluins start here

                //remove plugins if they were already added to the DOM
                if($('.plugin').length !== 0) {
                    $('.plugin').remove();
                }

                //add plugins dropdown
                $('#js-navbar').append('<li class="plugin"><a href="" class="dropdown-toggle" data-toggle="dropdown">Plugins<span class="caret"></span></a><ul class="dropdown-menu" id="plugin-dropdown" ></ul></li>');

                // Comments plugin - display model info in a modal
                this.comment_plugin = new Comment({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#commentModal">Model info</a></li>');
                $('body').append(this.comment_plugin.render().$el);

                // LCF sensitivity plugin - perform LCF sensitivity calculations
                this.LCFSensitivity_plugin = new LCFSensitivity({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#LCFSensitivityModal">LCF sensitivity</a></li>');
                $('body').append(this.LCFSensitivity_plugin.render().$el);
                
                // Oxidation sensitivity plugin
                this.OXSensitivity_plugin = new OXSensitivity({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#OXSensitivityModal">Oxidation sensitivity</a></li>');
                $('body').append(this.OXSensitivity_plugin.render().$el);                
                

                // Fracture Mechanics (1D) plugin - estimate remaining FM lifetime
                this.FM1D_plugin = new FM_1D({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#FM1DModal">1D Fracture Mechanics</a></li>');
                $('body').append(this.FM1D_plugin.render().$el);

                /*
                // Remaining lifetime estimation plugin
                this.RemLife_plugin = new RemLife({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#RemLifeModal">Remaining lifetime calculation</a></li>');
                $('body').append(this.RemLife_plugin.render().$el);      

                // Damage risk plugin
                this.DamageRisk_plugin = new DamageRisk({
                    model: displayModel
                });
                $('#plugin-dropdown').append('<li><a href="" data-toggle="modal" class="plugin" data-target="#DamageRiskModal">Damage risk assessment</a></li>');
                $('body').append(this.DamageRisk_plugin.render().$el); 
                */

            }, this);

            radio.reply('data:root', this.dataRoot);

            this.navbar = new Navbar({
                el: $('#jstree-tree'),
                model: new fileTree()
            });

            Backbone.history.start({pushState: true});
        },

        browse: function (path) {
            this.navbar.model.load(path);
        }
    });

    app.start();
});
