var compression = require('compression');
var express = require('express');
var fs = require('fs');
var path = require('path');

var app = express();

var dataPath = __dirname + "/data";

app.use(compression());
app.use(express.static(__dirname + '/app'));

var sendDataAtPath = function(req, res) {
    var requestPath = req.params[0] ? path.join(dataPath, req.params[0]) : dataPath;

    fs.stat(requestPath, function(err, stats) {
        if (err) { res.sendStatus(404); return };

        if (stats.isFile()) {
            res.sendFile(requestPath);
            return;
        };

        if (stats.isDirectory()) {
            var arr = fs.readdirSync(requestPath);
            var items = [];
            arr.forEach( function (el) {
                if (!el.startsWith(".")) {
                    items.push({
                        val: el,
                        cls: fs.statSync(path.join(requestPath, el)).isDirectory() ? "dir" : "file"
                    });
                };
            });
            res.json({items: items});
        };
    });
};

app.get('/data', sendDataAtPath);
app.get('/data/*?', sendDataAtPath);

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/app/index.html');
});

var port = process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});
