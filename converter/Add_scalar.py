from abaqusConstants import *
from odbAccess import *
odbPath = "blade_2.odb"         # path to output database
StepNumber = 12
FrameNumber = 1
# ******************************************************************************
odb = openOdb(odbPath,readOnly=FALSE)
Step=odb.steps.values()[StepNumber-1]
print 'Step name:', Step.name, 'Frame Number:',FrameNumber
Frame=Step.frames[FrameNumber]
testField2 = Frame.fieldOutputs['CE']
testField3=testField2.getSubset(position=ELEMENT_NODAL)
fieldValues=testField3.values
newField = Frame.FieldOutput(name='CE_NODES', description='CE at nodes', field=testField3)
odb.save()
odb.close()