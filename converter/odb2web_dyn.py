""" Usage: abq6XXX python odb2web.py ODB_NAME.odb
Converter works only with quadratic meshes of solid (C/DC 3D10, 3D15, 3D20) elements """
el_faces = {
    '4': [
        (1, 2, 3),
        (1, 4, 2),
        (2, 4, 3),
        (3, 4, 1)
        ],
    '6': [
        (1, 2, 3),
        (4, 6, 5),
        (1, 4, 5, 2),
        (2, 5, 6, 3),
        (3, 6, 4, 1)
        ],
    '8': [
        (1, 2, 3, 4),
        (5, 8, 7, 6),
        (1, 5, 6, 2),
        (2, 6, 7, 3),
        (3, 7, 8, 4),
        (4, 8, 5, 1)
        ],
    '10': [
        (1, 2, 3, 5, 6, 7),
        (1, 4, 2, 8, 9, 5),
        (2, 4, 3, 9, 10, 6),
        (3, 4, 1, 10, 8, 7)
        ],
    '15': [
        (1, 2, 3, 7, 8, 9),
        (4, 5, 6, 10, 11, 12),
        (1, 4, 5, 2, 13, 10, 14, 7),
        (2, 5, 6, 3, 14, 11, 15, 8),
        (3, 6, 4, 1, 15, 12, 13, 9)
        ],
    '20': [
        (1, 2, 3, 4, 9, 10, 11, 12),
        (5, 8, 7, 6, 16, 15, 14, 13),
        (1, 5, 6, 2, 17, 13, 18, 9),
        (2, 6, 7, 3, 18, 14, 19, 10),
        (3, 7, 8, 4, 19, 15, 20, 11),
        (4, 8, 5, 1, 20, 16, 17, 12)
        ]
    }

def getsum(lst):
    ''' Calculate sum of numbers in a list, built-in sum yields TypeError :/ '''
    result = 0
    for el in lst:
        result += el
    return result

def getFaces(elem):
    ''' Return a list of faces of a given element '''
    #if element is not a 3D solid for stress or thermal analysis, skip it
    if elem.type[:2] != 'DC' and elem.type[0] != 'C': return []

    p = re.compile(r'[A-Z]+3D([0-9]+)[A-Z]*') # regex to get number of nodes from eltype
    num_nodes = p.match(elem.type).group(1)
    if num_nodes not in el_faces: return []

    faces = []
    nodes = elem.connectivity
    for face_node_numbers in el_faces[num_nodes]:
        faces.append( tuple(nodes[i-1] for i in face_node_numbers) )
    return faces

def compareFaces(face1, face2):
    ''' Check if faces are the same by comparing their nodes '''
    for node in face1:
        if node not in face2:
            return False
    return True

def addFaces(src, dest):
    '''
    Add faces from src to dest, taking care of duplicates
    src - list of faces, where each face is a tuple of node numbers comprising the face
    dest - hashmap of faces, hashed by sum of face's nodes
    '''

    for s in src:
        hashsum = getsum(s)
        if hashsum in dest:
            for d in dest[hashsum]:
                if compareFaces(s,d):
                    dest[hashsum].remove(d)
                    break
            else:
                dest[hashsum].append(s)
        else:
            dest[hashsum] = [s]

def getIndex(a, x):
    ''' Efficiently find in list a the leftmost value exactly equal to x '''
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    raise ValueError

def getSurfMeshFromElements(elems, verbose=True):
    ''' Get a list of external faces of a given elements array '''

    if verbose: print 'Extracting surface mesh:     ',

    faces_dict = {}
    step = len(elems) / 100

    #key principle of surface faces selection
    #surface faces are attached to one element, inner - to two
    #the code below places all faces in array, if face is already in array, it removes it
    #thus only surface elements remain in the array
    for i, el in enumerate(elems):
        if verbose and i % step == 0:
            sys.stdout.write( '\b\b\b\b%3d%%' % (i/step) )
            sys.stdout.flush()
        addFaces(getFaces(el), faces_dict)

    if verbose: print

    return list( itertools.chain.from_iterable( faces_dict.itervalues() ) )

def getResultFromOdb(odb, resStep, resInc, fieldName, nodes):
    ''' Extract nodal results from odb given fieldOutput name and node list '''

    instance = odb.rootAssembly.instances.values()[0] # for now assume there is a single instance
    random_name = ''.join( random.sample('abcdefghijklmnopqrstuvwxyz', 10) )
    nset = instance.NodeSetFromNodeLabels(name = random_name, nodeLabels = nodes)
    
    field = odb.steps[resStep].frames[resInc].fieldOutputs[fieldName].getSubset(region=nset)

    result = []
    
    resultPosition = odb.steps[resStep].frames[resInc].fieldOutputs[fieldName].locations[0].position
    
    if (resultPosition!=NODAL)&(resultPosition!=ELEMENT_NODAL):
        print 'Only NODAL and ELEMENT_NODAL results positions are accepted'
        sys.exit(2)
        
    if resultPosition==NODAL:
        for value in field.values:
            result.append(value.magnitude)
            
    if resultPosition==ELEMENT_NODAL:
        
        nodeHash=[]
        nodeHashCount=[]
        maxNodeLabel=0
                
        for value in field.values:
            if value.nodeLabel>maxNodeLabel:
                maxNodeLabel=value.nodeLabel
        
        for k in range(0,maxNodeLabel+1):
            nodeHash.append(0)
            nodeHashCount.append(0)
        
        for value in field.values:
            #for tensor values
            #nodeHash[value.nodeLabel]+=value.mises;
            #for scalar values
            nodeHash[value.nodeLabel]+=value.data;
            nodeHashCount[value.nodeLabel]+=1;

        for k in range(0,maxNodeLabel+1):
            if(nodeHashCount[k]!=0):
                nodeHash[k]/=nodeHashCount[k]
                #remove unrealistic negative values
                if nodeHash[k] < 0:
                    nodeHash[k] = 0
        
        for kNode in nodes:
            result.append(nodeHash[kNode])
          
    return result

def getGeometryFromOdb(odb, setname):
    '''
    Extract external mesh from odb
    Returns position, nodenum, index
    position - list of node coordinates
    nodenum - corresponding node numbers
    index - list of node numbers comprising element faces
    '''
    instance = odb.rootAssembly.instances.values()[0] # for now assume there is a single instance
    if setname!='':
        elems = instance.elementSets[setname].elements
    else:
        elems = instance.elements

    faces = getSurfMeshFromElements(elems)

    print 'Creating connectivity data for lifehub...'

    nodes = sorted( set( itertools.chain.from_iterable(faces) ) )
    random_name = ''.join( random.sample('abcdefghijklmnopqrstuvwxyz', 10) )
    nset = instance.NodeSetFromNodeLabels(name = random_name, nodeLabels = nodes)

    # Create 'position' and 'nodenum' arrays
    position = []
    nodenum = []
    for n in nset.nodes:
        position += (n.coordinates * 1000.0).tolist()
        nodenum.append(n.label)

    # Create 'index' array
    index = []
    index_quad = []
    for face in faces:
        #do nor consider quadratic nodes in face indices (connectivity array)
        if (len(face) == 6) or (len(face) == 3):
           face_index = [getIndex(nodenum, node) for node in face[0:3]]
        else:
           face_index = [getIndex(nodenum, node) for node in face[0:4]]
        if len(face_index) == 3:
            index += face_index
        else:
            index_quad += face_index
    #result of getGeometryFromOdb: a dictionary with the following information:
    geometry = {
        #array of node coordinates
        'position': position,
        #array of ABAQUS node numbers
        'nodenum': nodenum,
        #array of arrays of face-forming node numbers (triangular faces)
        'index': index,
        #array of arrays of face-forming node numbers (quad faces)
        'index_quad': index_quad
        }

    return geometry


if __name__ == '__main__':
    from odbAccess import *
    import sys
    import itertools
    import random
    import re
    from bisect import bisect_left
    import json
    from array import array
    import umsgpack

    #if insufficient number of arguments was supplied
    if len(sys.argv) < 2:
        print __doc__
        sys.exit(2)

    json_name = sys.argv[1]
  
    with open(json_name) as json_data:
        json_d = json.load(json_data)

    if len(json_d['results']) < 1:
        print('No results found')
        sys.exit(2)

    #open ABAQUS ODB file
    odbname = str(json_d['odb'])
    if 'elset' in json_d:    
        setname = str(json_d['elset'])
    else:
        setname = ''
    odb = openOdb(odbname, readOnly=True)

    #Get geometry - see function above
    geometry = getGeometryFromOdb(odb, setname)
    odb.close()

    #Get specified results
    print 'Extracting results...'

    numResults = len(json_d['results'])

    print 'Results number:', numResults

    results = {}

    for j in range(1,numResults+1):
        odbname = str(json_d['results'][j-1]['odb'])
        resName = str(json_d['results'][j-1]['name'])
        resNameWrite = str(json_d['results'][j-1]['resultName'])
        resStep = str(json_d['results'][j-1]['step'])
        resInc  = int(json_d['results'][j-1]['increment'])
        print(resName)
        odb = openOdb(odbname, readOnly=True)
        results[resNameWrite] = getResultFromOdb(odb, resStep, resInc, resName, geometry['nodenum'])
        odb.close()

    print 'Arrays created successfully'

    #create object with data for lifehub


    data = {
        u'data': {
            u'attributes': {
                u'position': { u'itemSize': 3, u'type': u'Float32Array', u'array': array('f', geometry['position']).tostring() },
                u'nodenum': { u'itemSize': 1, u'type': u'Uint32Array', u'array': array('I', geometry['nodenum']).tostring() },
                u'index_quad': { u'itemSize': 1, u'type': u'Uint32Array', u'array': array('I', geometry['index_quad']).tostring() }
                },
            u'index': { u'type': u'Uint32Array', u'array': array('I', geometry['index']).tostring() },
            }
        }

    #if name and comment are present, write them into model pack file

    if 'name' in json_d:
        model_name = json_d['name']
        data[u'model_name'] = model_name

    if 'comment' in json_d:
        comment = json_d['comment']
        data[u'comment'] = comment

    #if material data is available, write into model pack file

    if 'material_data' in json_d:
        data[u'material_data'] = json_d['material_data']


    #write all results
    for i in range(1, numResults+1):
        resNameWrite = str(json_d['results'][i-1]['resultName'])
        data[u'data'][u'attributes'][unicode(resNameWrite)] = { u'itemSize': 1, u'type': u'Float32Array', u'array': array('f', results[resNameWrite]).tostring() }

    print 'Writing lifehub data file...'
    fout = open(odbname + '.pack','wb')
    umsgpack.dump(data, fout)
    fout.close()